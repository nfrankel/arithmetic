package com.sap.ps.expertise.yaas;

public class YaasContext
{
	@javax.validation.constraints.Pattern(regexp="^[a-z][a-z0-9]+$")
	@javax.validation.constraints.Size(min=3,max=16)
	@javax.ws.rs.HeaderParam("hybris-tenant")
	private String hybrisTenant;

	@javax.validation.constraints.Pattern(regexp="^[a-z][a-z0-9-]{1,14}[a-z0-9][.][a-z][a-z0-9-]{0,22}[a-z0-9]$")
	@javax.validation.constraints.Size(min=6,max=41)
	@javax.ws.rs.HeaderParam("hybris-client")
	private String hybrisClient;

	@javax.ws.rs.HeaderParam("hybris-user")
	private String hybrisUser;

	@javax.validation.constraints.Pattern(regexp="^([a-zA-Z0-9._=-]{1,128}( [a-zA-Z0-9._=-]{1,128})*)?$")
	@javax.ws.rs.HeaderParam("hybris-scopes")
	private String hybrisScopes;

	@javax.ws.rs.HeaderParam("hybris-request-id")
	private String hybrisRequestId;

	@javax.validation.constraints.DecimalMin(value="1")
	@javax.ws.rs.DefaultValue("1")	@javax.ws.rs.HeaderParam("hybris-hop")
	private Integer hybrisHop;

	@SuppressWarnings("unused")
	public String getHybrisTenant()
	{
		return hybrisTenant;
	}

	@SuppressWarnings("unused")
	public String getHybrisClient()
	{
		return hybrisClient;
	}

	@SuppressWarnings("unused")
	public String getHybrisUser()
	{
		return hybrisUser;
	}

	@SuppressWarnings("unused")
	public String getHybrisScopes()
	{
		return hybrisScopes;
	}

	@SuppressWarnings("unused")
	public String getHybrisRequestId()
	{
		return hybrisRequestId;
	}

	@SuppressWarnings("unused")
	public Integer getHybrisHop()
	{
		return hybrisHop;
	}

	@SuppressWarnings("unused")
	public void setHybrisTenant(final String hybrisTenant)
	{
		this.hybrisTenant = hybrisTenant;
	}

	@SuppressWarnings("unused")
	public void setHybrisClient(final String hybrisClient)
	{
		this.hybrisClient = hybrisClient;
	}

	@SuppressWarnings("unused")
	public void setHybrisUser(final String hybrisUser)
	{
		this.hybrisUser = hybrisUser;
	}

	@SuppressWarnings("unused")
	public void setHybrisScopes(final String hybrisScopes)
	{
		this.hybrisScopes = hybrisScopes;
	}

	@SuppressWarnings("unused")
	public void setHybrisRequestId(final String hybrisRequestId)
	{
		this.hybrisRequestId = hybrisRequestId;
	}

	@SuppressWarnings("unused")
	public void setHybrisHop(final Integer hybrisHop)
	{
		this.hybrisHop = hybrisHop;
	}
}
