package com.sap.ps.expertise.yaas;

import com.sap.cloud.yaas.servicesdk.authorization.cache.SimpleCachingProviderWrapper;
import com.sap.cloud.yaas.servicesdk.authorization.integration.AuthorizedExecutionTemplate;
import com.sap.cloud.yaas.servicesdk.authorization.protocol.ClientCredentialsGrantProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ArithmeticApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArithmeticApplication.class, args);
    }

    @Bean
    public AuthorizedExecutionTemplate authorizedExecutionTemplate() {
        return new AuthorizedExecutionTemplate(new SimpleCachingProviderWrapper(new ClientCredentialsGrantProvider()));
    }
}
