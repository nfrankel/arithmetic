package com.sap.ps.expertise.yaas;

import com.sap.cloud.yaas.servicesdk.authorization.AuthorizationScope;
import com.sap.cloud.yaas.servicesdk.authorization.DiagnosticContext;
import com.sap.cloud.yaas.servicesdk.authorization.integration.AuthorizedExecutionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import java.util.Collections;

@Component
@Path("/add")
public class ArithmeticEndpoint {

    private final static String DEFAULT_BASE_URI = "https://api.yaas.io/frankel/random/v1/get";
    private final static AuthorizationScope SCOPE = new AuthorizationScope(Collections.singletonList("frankel.random_get"));

    @Autowired
    public AuthorizedExecutionTemplate template;

    @GET
    @Produces({"application/json"})
    public Response add(@BeanParam @Valid YaasContext context) {
        int i1 = getRandom(context);
        int i2 = getRandom(context);
        return Response.ok(new Sum(i1, i2)).build();
    }

    private int getRandom(YaasContext context) {
        return template.executeAuthorized(SCOPE, new DiagnosticContext(context.getHybrisRequestId(), context.getHybrisHop()), token -> {
            Client client = ClientBuilder.newClient();
            Invocation invocation = client.target(DEFAULT_BASE_URI).request().buildGet();
            Response response = invocation.invoke();
            String entity = response.readEntity(String.class);
            response.close();
            return Integer.parseInt(entity);
        });
    }
}
