package com.sap.ps.expertise.yaas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Sum {

    private final int i1;
    private final int i2;
    private final int sum;

    public Sum(int i1, int i2) {
        this.i1 = i1;
        this.i2 = i2;
        sum = i1 + i2;
    }

    public int getI1() {
        return i1;
    }

    public int getI2() {
        return i2;
    }

    public int getSum() {
        return sum;
    }
}
