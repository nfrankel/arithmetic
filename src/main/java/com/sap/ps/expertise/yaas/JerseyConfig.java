package com.sap.ps.expertise.yaas;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(ArithmeticEndpoint.class);
    }
}